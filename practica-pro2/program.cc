//
//  program.cc
//  practica-pro2
//
//  Created by Alvaro Ariño Cabau on 04/11/2018.
//  Copyright © 2018 Alvaro Ariño Cabau. All rights reserved.
//

/**
 * @mainpage Práctica de PRO2. Otoño 2018.
 
 Práctica por <b>Alvaro Ariño</b>. Grupo 14.
 
 <H3>Descripción de la práctica</H3>
 Queremos simular el sistema de gestión de tareas de una arquitectura mutiprocesador y multiusuario tipo NUMA, donde cada procesador trabaja exclusivamente con su propia memoria y puede ejecutar más de un proceso simultáneamente.
 
 <H3>Estructura de la práctica</H3>
 El programa principal se encuentra en el módulo main.cc. Atendiendo a los tipos de datos sugeridos en el enunciado y conseguir la simulación del sistema de gestión de tareas de una arquitectura mutiprocesador y multiusuario tipo NUMA, necesitaremos un módulo para:
 - Representar el <em>Cluster</em> de procesadores.
 - Gestionar cada <em>Procesador</em> del Cluster.
 - Gestionar el <em>Usuario</em>. Para la gestión de todos los usuarios que tengan acceso al cluster se usará un <em>Cjt_usuarios</em>.
 - Gestionar el <em>Proceso</em> que se ejecuta.
 
*/

/** @file program.cc
    @brief Código del programa principal.
*/

#include "cluster.hh"
#include "Cjt_usuarios.hh"

#ifndef NO_DIAGRAM  
#include <iostream>
#include <string>
using namespace std;
#endif

int main() {
    
    Cluster c;
    Cjt_usuarios cjt;
    
    string x;
    
    cin >> x;
    while (x != "acabar") {
        if (x == "configurar_cluster") {
            c.configurar_cluster();
            
        } else if (x == "poner_usuario") {
            string u_id; // Id del usuario
            cin >> u_id;
            cjt.poner_usuario(u_id);
            
        } else if (x == "quitar_usuario") {
            string u_id; // Id del usuario
            cin >> u_id;
            cjt.quitar_usuario(u_id);
            
        } else if (x == "poner_proceso_en_procesador") {
            int pro_id; // Id del procesador
            int id, mem, t; // Parámetros del proceso
            string u_id; // Id del usuario
            cin >> pro_id >> u_id >> id >> mem >> t;
            Proceso p (id, mem, t);
            c.poner_proceso_en_procesador(p, cjt, u_id, pro_id);
            
        } else if (x == "quitar_proceso_de_procesador") {
            int p_id, pro_id; // Id del proceso, Id del procesador
            cin >> pro_id >> p_id;
            c.quitar_proceso_de_procesador(p_id, pro_id);
            
        } else if (x == "enviar_proceso_a_usuario") {
            string u_id; // Id del usuario
            int id, mem, t; // Parámetros del proceso
            cin >> u_id >> id >> mem >> t;
            Proceso p (id, mem, t);
            cjt.enviar_proceso_a_usuario(p, u_id);
            
        } else if (x == "consultar_usuario") {
            string u_id; // Id del usuario
            cin >> u_id;
            cjt.consultar_usuario(u_id);
            
        } else if (x == "avanzar_tiempo") {
            int t; // Tiempo
            cin >> t;
            c.avanzar_tiempo(t);
            
        } else if (x == "consultar_procesador") {
            int pro_id; // Id del procesador
            cin >> pro_id;
            c.consultar_procesador(pro_id);
            
        } else if (x == "enviar_procesos_a_cluster") {
            int np; // Numero de procesos
            cin >> np;
            c.enviar_procesos_a_cluster(np, cjt);
            
        }
        
        cin >> x;
    }
    
}
