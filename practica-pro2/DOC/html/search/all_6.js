var searchData=
[
  ['práctica_20de_20pro2_2e_20otoño_202018_2e',['Práctica de PRO2. Otoño 2018.',['../index.html',1,'']]],
  ['poner_5fproceso_5fen_5fprocesador',['poner_proceso_en_procesador',['../class_procesador.html#a5c4247d5f4cea97df38d9a44726d40b3',1,'Procesador']]],
  ['poner_5fusuario',['poner_usuario',['../class_cjt__usuarios.html#a23f6762713fa6800cb34211715d6c6fc',1,'Cjt_usuarios']]],
  ['procesador',['Procesador',['../class_procesador.html',1,'Procesador'],['../class_procesador.html#aacfa2a06c7d44636ecfb0338bebbfa8b',1,'Procesador::Procesador()'],['../class_procesador.html#a8fcdb116e68accc213610b95b50a07a5',1,'Procesador::Procesador(int mem)']]],
  ['procesador_2ehh',['procesador.hh',['../procesador_8hh.html',1,'']]],
  ['proceso',['Proceso',['../class_proceso.html',1,'Proceso'],['../class_proceso.html#a632f57d6ec48e76c80fbeb7734c1148c',1,'Proceso::Proceso()'],['../class_proceso.html#a18aa1fd6faed2e77106d667b48489197',1,'Proceso::Proceso(int mem, string id, Usuario user, int t)']]],
  ['proceso_2ehh',['proceso.hh',['../proceso_8hh.html',1,'']]]
];
