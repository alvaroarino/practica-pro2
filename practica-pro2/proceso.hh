//
//  proceso.hh
//  practica-pro2
//
//  Created by Alvaro Ariño Cabau on 05/11/2018.
//  Copyright © 2018 Alvaro Ariño Cabau. All rights reserved.
//

/** @file proceso.hh
 @brief Especificación de la clase Proceso
 */

#ifndef proceso_hh
#define proceso_hh

#ifndef NO_DIAGRAM
#include <iostream>
using namespace std;
#endif

/** @class Proceso
 @brief Representa un proceso con un id, usuario, cantidad de memoria reservada y tiempo estimado asociados.
 
 Dispone de dos estados posibles (inicializada / no inicializada); si está inicializada tiene una cantidad de memoria asociado, un identificador, un usuario propietario y el tiempo estimado que va a necesitar; si no está inicializada los valores del parámetro implicito son 0.
 
 Un proceso a la espera de ser ejecutado recibe el nombre de <em>pendiente</em>, cuando este llegue al cluster pasará a llamarse <em>activo</em>.
 
 */

class Proceso {
private:
    /** @brief Memoria que ocupa el proceso. */
    int mem;
    
    /** @brief ID del proceso. */
    int id;
    
    /** @brief Tiempo del proceso. */
    int t;
    
public:
    
    // Constructores
    /** @brief Creadora por defecto.
     
     Se ejecuta automáticamente al declarar un proceso.
     \pre <em>cierto</em>
     \post El resultado es un proceso
     */
    Proceso();
    
    /** @brief Creadora con identificador de proceso.
     
     Se ejecuta automáticamente al declarar un proceso.
     \pre <em>cierto</em>
     \post El resultado es un proceso
     */
    Proceso(int ID);
    
    /** @brief Creadora con memoria, identificador de proceso y tiempo de inicio
     
     Se ejecuta automáticamente al declarar un proceso con memoria, identificador de proceso y tiempo de inicio
     \pre <em>cierto</em>
     \post El resultado es un proceso con distintos valores asociados
     */
    Proceso(int ID, int memoria, int tiempo);
    
    // Modificadoras
    /** @brief Modificar el tiempo del proceso
     \pre El proceso tiene t > 0.
     \post El resultado es el tiempo origianl menos el parámetro t.
     */
    void modificar_tiempo(int t);
    
    // Consultoras
    /** @brief Consultora del id de proceso
     \pre El proceso tiene un id asociado, sino se devuleve 0
     \post El resultado es el id asociado al proceso
     */
    int id_del_proceso() const;
    
    /** @brief Consultar si el proceso está vacio
     \pre cierto
     \post El resultado es si el proceso está vacio o no
     */
    bool empty() const;
    
    /** @brief Consultar la memoria que ocupa
     \pre cierto
     \post El resultado es la memoria que ocupa
     */
    int memoria() const;
    
    /** @brief Consultar el tiempo asociado al proceso
     \pre cierto
     \post El resultado es el tiempo del proceso
     */
    int tiempo() const;
    
    // Lectura y escritura    
    /** @brief Escritura del proceso mediante entrada estandard
     \pre cierto
     \post se
     */
    void escribir() const;
};

#endif
