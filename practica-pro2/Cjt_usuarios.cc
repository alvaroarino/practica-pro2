//
//  Cjt_usuarios.cc
//  practica-pro2
//
//  Created by Alvaro Ariño Cabau on 19/11/2018.
//  Copyright © 2018 Alvaro Ariño Cabau. All rights reserved.
//

/** @file Cjt_usuarios.cc
 @brief Código de la clase Cjt_usuarios
 */

#include "Cjt_usuarios.hh"

Cjt_usuarios::Cjt_usuarios() {
    muser = map<string, Usuario> ();
    procesos_pendietes = 0;
}

void Cjt_usuarios::poner_usuario(const string id) {
    CjtIt it = muser.find(id);
    if (it == muser.end()) {
        Usuario user;
        pair<string, Usuario> x = make_pair(id, user);
        muser.insert(x);
    }
}

void Cjt_usuarios::quitar_usuario(string id) {
    CjtIt it;
    it = muser.find(id);
    if (it != muser.end() and it -> second.pendientes_y_mas_antiguo().first == 0) {
        muser.erase(it);
    }
}

void Cjt_usuarios::enviar_proceso_a_usuario(Proceso p, string id) {
    CjtIt it = muser.find(id);
    if (it != muser.end()) {
        it -> second.enviar_proceso_a_usuario(p);
    }
    recalcular_procesos_pendientes();
}

void Cjt_usuarios::enviar_proceso_rechazado(Proceso p, string id) {
    CjtIt it = muser.find(id);
    if (it != muser.end()) {
        it -> second.enviar_proceso_rechazado(p);
    }
}

void Cjt_usuarios::recalcular_procesos_pendientes() {
    int x = 0;
    for (CjtIt it = muser.begin(); it != muser.end(); ++it) {
        int y = it -> second.pendientes_y_mas_antiguo().first;
        x += y;
    }
    procesos_pendietes = x;
}

Proceso Cjt_usuarios::obtener_proceso_pendiente(string id) {
    CjtIt it = muser.find(id);
    Proceso p;
    
    if (it != muser.end() and it -> second.pendientes_y_mas_antiguo().first != 0) {
        p = it -> second.proceso_mas_antiguo();
        --procesos_pendietes;
        return p;
    }
    
    return p;
}

void Cjt_usuarios::reset_rechazados_cjt() {
    for (CjtIt it = muser.begin(); it != muser.end(); ++it) {
        it -> second.reset_rechazados();
    }
}

int Cjt_usuarios::size() const {
    return int(muser.size());
}

bool Cjt_usuarios::empty() const {
    return muser.empty();
}

int Cjt_usuarios::numero_procesos_pendientes() const {
    return this -> procesos_pendietes;
}

void Cjt_usuarios::consultar_usuario(string id) const {
    cout << "Usuario " << id << endl;
    CjtItc it = muser.begin();
    it = muser.find(id);
    if (it != muser.end()) {
        pair<int, int> datos = consultar_datos(id);
        if (datos.first != 0) cout << "  " << datos.first << " " << datos.second << endl;
        else cout << "  " << datos.first << endl;
    }
}

pair<int, int> Cjt_usuarios::consultar_datos(string id) const {
    CjtItc it;
    it = muser.find(id);
    if (it -> second.empty()) return make_pair(0, 0);
    else return it -> second.pendientes_y_mas_antiguo();
}

bool Cjt_usuarios::existencia_usuario(string id) const {
    CjtItc it = muser.find(id);
    muser.find(id);
    if (it != muser.end()) return true;
    else return false;
}

string Cjt_usuarios::obtener_id() const {
    
    string sol = "";
    int max = 0;
    
    for (CjtItc it = muser.begin(); it != muser.end(); ++it) {
        int pendientes = it -> second.pendientes_y_mas_antiguo().first;
        if (pendientes > max) {
            sol = it -> first;
            max = pendientes;
        }
    }
    
    return sol;
}
