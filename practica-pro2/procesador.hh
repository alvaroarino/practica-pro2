//
//  procesador.hh
//  practica-pro2
//
//  Created by Alvaro Ariño Cabau on 05/11/2018.
//  Copyright © 2018 Alvaro Ariño Cabau. All rights reserved.
//

/** @file procesador.hh
    @brief Especificación de la clase Procesador
*/
#ifndef procesador_hh
#define procesador_hh

#include "proceso.hh"
#include "usuario.hh"
#include "Cjt_usuarios.hh"

#ifndef NO_DIAGRAM
#include <iostream>
#include <list>
using namespace std;
#endif

/** @class Procesador
    @brief Representa un procesador con una cantidad de memora asignada.
 
    Dispone de dos estados posibles (inicializada / no inicializada); si está inicializada tiene una cantidad de memoria asociado; si no está inicializada no tiene memoria asignada.
 
    Todas las operaciones son de <b>coste constante</b> salvo las indicadas.
 */

class Procesador {
private:
    /** @brief Estructura interna de la memoria */
    struct MemData {
        /** @brief Posición inicial */
        int pos;
        
        /** @brief Posición final */
        int end;
        
        /** @brief Posición de memoria vacia */
        bool vacia;
        
        /** @brief Proceso en la memoria */
        Proceso p;
        
        /** @brief Usuario propietario del proceso */
        string user;
    };
    
    /** @brief Memoria disponible */
    list<MemData> mem;
    
    /** @brief Memoria total */
    int MAX_MEM;
    
    /** @brief Memoria libre */
    int tam_mem_libre;
    
    /** @brief Tipo de Iterador de la memoria */
    typedef list<MemData>::iterator MemIt;
    
    /** @brief Tipo de Iterador constante de la memoria */
    typedef list<MemData>::const_iterator MemItc;
    
    /** @brief Recalcular memoria libre
     
     \pre cierto
     \post tam_mem_libre queda actualizado con la memoria libre en ese momento.
     */
    void recalcular_mem_libre();
    
    /** @brief Desfragmentar la memoria
     
     \pre cierto
     \post Los fragmentos de memoria vacia consecutivos quedan unidos.
     */
    void desfragmentar_memoria();
    
public:
    
    // Contructoras
    /** @brief Creadora por defecto.
     
     Se ejecuta automáticamente al declarar un procesador.
     \pre <em>cierto</em>
     \post El resultado es un procesador sin memoria asignada
     */
    Procesador();
    
    /** @brief Creadora con memoria.
     
     Se ejecuta automáticamente al declarar un procesador con memoria.
     \pre <em>cierto</em>
     \post El resultado es un procesador con memoria asignada
     */
    Procesador(int m);
    
    // Modificador
    /** @brief Añadir un proceso al procesador.
     
     \pre Existe el procesador y usuario indicado. Si el usuario no existe no se hace nada.
     \post Si hay un hueco de memoria libre consecutiva en el que quepa el proceso (new_p),
     este pasa a ejecutarse en el procesador indicado y la memoria que use pasa a estar ocupada.
     Si el proceso no cabe, pasa a ser el proceso pendiente mas reciente del usuario (id).
     */
    void poner_proceso(Proceso& new_p, Cjt_usuarios& cjt, string id);
    
    /** @brief Quitar un proceso del procesador.
     
     \pre Existe el proceso indicado. Si no existe no se hace nada.
     \post Si existe el proceso, la memoria usada por este queda libre.
     */
    void quitar_proceso(int id_proceso);
    
    /** @brief Avanazar el tiempo
     
     \pre El parámetro implícito no está vacio.
     \post El parámetro implícito contiene todos los procesos salvo aquellos que estaban activos y que hayan acabado en el intervalo de tiempo indicado desde el momento inicial.
     */
    void avanzar_tiempo_procesador(int t);
    
    // Consultoras
    /** @brief Consultar si hay procesos
     
     \pre cierto
     \post Devuelve true si no hay procesos
     */
    bool empty() const;
    
    /** @brief Consultar los huecos de memoria libres
     
     \pre cierto.
     \post Devuleve si hay algún hueco de memoria libre donde entre el proceso.
     */
    bool consultar_memoria(Proceso& p) const;
    
    /** @brief Consultar el tamaño de la memoria libre del procesador
     
     \pre cierto.
     \post Devuleve el valor de memoria libre del procesador.
     */
    int memoria_libre() const;
    
    // Lectura y escritura
    
    /** @brief Escritura del conjunto por el canal estandard
     
     \pre cierto.
     \post Se escribe por orden de inicio de memoria, la posicion y el resto de datos de cada proceso.
     El tiempo escrito es el que falta para que el proceso acabe, no el tiempo inicial de ejecución.
     */
    void escribir() const;
};

#endif
