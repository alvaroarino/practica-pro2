//
//  cluster.cc
//  practica-pro2
//
//  Created by Alvaro Ariño Cabau on 19/11/2018.
//  Copyright © 2018 Alvaro Ariño Cabau. All rights reserved.
//

/** @file cluster.cc
 @brief Código de la clase Cluster
 */

#include "cluster.hh"

BinTree<int> Cluster::build_tree() {
    BinTree<int> relacion_de_procesadores;
    int n;
    cin >> n;
    if (n != 0) {
        // En preorden
        BinTree<int> i = build_tree();
        BinTree<int> d = build_tree();

        relacion_de_procesadores = BinTree<int>(n, i, d);
    }
    return relacion_de_procesadores;
}

int Cluster::sumar_total_mem_libre() const {
	int m = 0;
    for (int i = 0; i < procesadores.size(); ++i) {
        m += procesadores[i].memoria_libre();
    }
	return m;
}

int Cluster::desempate(int id1, int id2, int prof1, int prof2) {
	if (prof1 > prof2) return id2;
	else return id1;
}

int Cluster::i_eleccion_de_procesador(const BinTree<int>& a, const vector<Procesador>& v, int profundidad_izquierdo, int profundidad_derecha) {
	
	int id1, id2, r;
	
	id1 = id2 = r = a.value();
	
	if (not a.left().empty()) {
		int i = i_eleccion_de_procesador(a.left(), v, profundidad_izquierdo + 1, profundidad_derecha);
		if (v[i - 1].memoria_libre() > v[r - 1].memoria_libre()) id1 = i;
		else if (v[i - 1].memoria_libre() < v[r - 1].memoria_libre()) id1 = r;
		else {
			id1 = desempate(i, r, profundidad_izquierdo, profundidad_derecha);
		}
	}
	
	if (not a.right().empty()) {
		int d = i_eleccion_de_procesador(a.right(), v, profundidad_izquierdo, profundidad_derecha + 1);
		if (v[d - 1].memoria_libre() > v[r - 1].memoria_libre()) id2 = d;
		else if (v[d - 1].memoria_libre() < v[r - 1].memoria_libre()) id2 = r;
		else {
			id2 = desempate(d, r, profundidad_izquierdo, profundidad_derecha);
		}
	}
	
	if (v[id1 - 1].memoria_libre() > v[id2 - 1].memoria_libre()) return id1;
	else if (v[id1 - 1].memoria_libre() < v[id2 - 1].memoria_libre()) return id2;
	else {
		return desempate(id1, id2, profundidad_izquierdo, profundidad_derecha);
	}
}

int Cluster::eleccion_de_procesador(const BinTree<int>& a, const vector<Procesador>& v) {
	return i_eleccion_de_procesador(a, v, 0, 0);
}

Cluster::Cluster() {
    relacion_de_procesadores = BinTree<int> ();
    procesadores = vector<Procesador>();
	total_mem_libre = 0;
}

void Cluster::configurar_cluster() {
    
    // cout << "Introducir el número de procesadores: ";
    
    int n;
    cin >> n;
    
    if (not relacion_de_procesadores.empty()) {
        // Vaciar el parámetro implicito si estaba lleno
        relacion_de_procesadores = BinTree<int> ();
    }
    procesadores = vector<Procesador> (n);
    
    // cout << "Introduzca los ID de procesador:" << endl;
    relacion_de_procesadores = build_tree();
    
    // cout << "Parametro implicito T listo" << endl;
	
    // cout << "Introducir los datos de memora de cada procesador: ";
    for (int i = 0; i < procesadores.size(); ++i) {
        int mem;
        cin >> mem;
        Procesador pro(mem);
        procesadores[i] = pro;
    }
	
	// Actualizamos el valor del total de memoria libre del cluster
	total_mem_libre = sumar_total_mem_libre();
	
    // cout << "Cluster configurado" << endl;
    
}

void Cluster::poner_proceso_en_procesador(Proceso& p, Cjt_usuarios& cjt, string id, int id_del_procesador) {
    if (cjt.existencia_usuario(id)) {
		if (procesadores[id_del_procesador - 1].consultar_memoria(p)) {
			procesadores[id_del_procesador - 1].poner_proceso(p, cjt, id);
			// Actualizamos el valor del total de memoria libre del cluster
			total_mem_libre = sumar_total_mem_libre();
		} else {
			cjt.enviar_proceso_a_usuario(p, id);
		}
    }
    
}

void Cluster::quitar_proceso_de_procesador(int id_proceso, int id_del_procesador) {
	// Si no hay procesos no hacemos nada
	if (not procesadores[id_del_procesador - 1].empty()) {
		
		procesadores[id_del_procesador - 1].quitar_proceso(id_proceso);
		
		// Actualizamos el valor del total de memoria libre del cluster
		total_mem_libre = sumar_total_mem_libre();
	}
}

void Cluster::avanzar_tiempo(int t) {
    if (not relacion_de_procesadores.empty()) {
        for (int i = 0; i < procesadores.size(); ++i) {
			if (not procesadores[i].empty()) {
				procesadores[i].avanzar_tiempo_procesador(t);
			}
        }
		// Actualizamos el valor del total de memoria libre del cluster
		total_mem_libre = sumar_total_mem_libre();
    }
}

void Cluster::enviar_procesos_a_cluster(int np, Cjt_usuarios& cjt) {
	if (not relacion_de_procesadores.empty() and not cjt.empty()) {
		
		while (np != 0) {
			// cout << "Entrando en el bucle" << endl;
			
			// cout << "Seleccionando usuario..." << endl;
			string user_id = cjt.obtener_id();
			// cout << "Usuario seleccionado: " << user_id << endl;

			if (user_id == "" or total_mem_libre == 0) np = 0;
			else {
				// cout << "Obteniendo proceso..." << endl;
				Proceso p = cjt.obtener_proceso_pendiente(user_id);
				// cout << "Proceso obtenido: ";
				// p.escribir();
				
				// cout << "Obteniendo ID de procesador..." << endl;
				int id_del_procesador = eleccion_de_procesador(relacion_de_procesadores, procesadores);
				// cout << "ID obtenido: " << id_del_procesador << endl;
				
				if (procesadores[id_del_procesador - 1].consultar_memoria(p)) {
					// cout << "Enviando proceso a memoria..." << endl;
					poner_proceso_en_procesador(p, cjt, user_id, id_del_procesador);
					// cout << "Proceso enviado" << endl;
					--np;
				} else {
					cjt.enviar_proceso_rechazado(p, user_id);
				}
			}
			// cout << "Procesos pendientes: " << np << endl;
		}
		// Actualizamos el valor del total de memoria libre del cluster
		total_mem_libre = sumar_total_mem_libre();
		cjt.reset_rechazados_cjt();
	}
}


void Cluster::consultar_procesador(int id_del_procesador) const {
	if (not relacion_de_procesadores.empty()) {
		if (id_del_procesador - 1 < procesadores.size()) {
			cout << "Procesador " << id_del_procesador << endl;
			if (id_del_procesador - 1 != 0 and procesadores.size() != 0) {
				if (not procesadores[id_del_procesador - 1].empty()) {
					procesadores[id_del_procesador - 1].escribir();
				}
			}
		}
	}
}
