//
//  Cjt_usuarios.hh
//  practica-pro2
//
//  Created by Alvaro Ariño Cabau on 09/11/2018.
//  Copyright © 2018 Alvaro Ariño Cabau. All rights reserved.
//

/** @file Cjt_usuarios.hh
    @brief Especificación de la clase Cjt_usuarios
*/
#ifndef cjt_usuario_hh
#define cjt_usuario_hh

#include "proceso.hh"
#include "usuario.hh"

#ifndef NO_DIAGRAM
#include <iostream>
#include <map>
using namespace std;
#endif

/** @class Cjt_usuarios
    @brief Representa un conjunto de usuarios ordenados por id
 
    Solo dispone de un estado posible (no inicializada); el parámetro implícito no contiene ningún usuario.
    Los usuarios se añaden mediante la acción designada para ello.
 */

class Cjt_usuarios {
private:
    
    /** @brief Iterador del diccionario */
    typedef map<string, Usuario>::iterator CjtIt;
    
    /** @brief Iterador constante del diccionario */
    typedef map<string, Usuario>::const_iterator CjtItc;
    
    /** @brief Conjunto de usuarios*/
    map<string, Usuario> muser;
    
    /** @brief Procesos pendientes del conjunto*/
    int procesos_pendietes;
    
public:
    
    // Constructoras
     /** @brief Creadora por defecto.
     
     Se ejecuta automáticamente al declarar un usuario.
     \pre <em>cierto</em>
     \post el resultado es un conjunto de Usuarios
     */
    Cjt_usuarios();
    
    // Modificadores
    /** @brief Modificadora para añadir un usuario
      \pre El parametro implícito no tiene el usuario. Si lo tiene no hace nada.
      \post El parámetro implícito pasa a tener el id del usuario.
    */
    void poner_usuario(const string id);
    
    /** @brief Modificadora para eliminar un usuario
      \pre el parametro implícito tiene el usuario sin procesos pendientes. Si no existe el usuario o le quedaban procesos pendientes no se hace nada.
      \post El usuario se elimina del parámetro implícito.
    */
    void quitar_usuario(string id);
    
    /** @brief Envio de un proceso a un usuario especifico
     \pre el parametro implícito tiene el usuario. Si no existe el usuario no se hace nada.
     \post El usuario pasa a tener el proceso p como pendiente.
     */
    void enviar_proceso_a_usuario(Proceso p, string id);
    
    /** @brief Envio de un proceso a un usuario especifico
     \pre el parametro implícito tiene el usuario. Si no existe el usuario no se hace nada.
     \post El usuario pasa a tener el proceso p como pendiente. El número de rechazados aumenta en 1 cada vez que se envia uno.
     */
    void enviar_proceso_rechazado(Proceso p, string id);
    
    /** @brief Recalcular el número de procesos pendientes del conjunto
     \pre cierto
     \post Recalcula el número de procesos pendientes del conjunto
     */
    void recalcular_procesos_pendientes();
    
    /** @brief Consultar el proceso pendiente mas antiguo del usuario
     \pre cierto
     \post devuleve el proceso pendiente mas antiguo del usuario, este queda eliminado del usuario.
     */
    Proceso obtener_proceso_pendiente(string id);
    
    /** @brief Resetear el contador de procesos rechazados de los usuarios
     \pre cierto
     \post Reinicia el número de procesos rechazados de los usuarios
     */
    void reset_rechazados_cjt();
    
    // Consultoras
    /** @brief Número de usuarios.
     \pre cierto
     \post Devuelve el número de usuarios que hay en el parámetro implícito.
     */
    int size() const;
    
    /** @brief Consultar si el parámetro implícito está vacio.
     \pre cierto
     \post Devuelve true si el parámetro implícito está vacio.
     */
    bool empty() const;
    
    /** @brief Consultar el número de procesos pendientes del procesador.
     \pre cierto
     \post Devuelve el número de procesos pendientes del procesador.
     */
    int numero_procesos_pendientes() const;
    
    /** @brief Consultar del estudiante con id del conjunto
      \pre el parametro implícito tiene el usuario.
      \post Si el usuario existe se escribe cuantos procesos pendientes tiene y el identificador del mas antiguo.
    */
    void consultar_usuario(string id) const;
    
    /** @brief Consultar datos del usuario
     \pre <em>cierto</em>
     \post El resultado es cuantos procesos pendientes tiene y el identificador del mas antiguo. Si no tiene procesos pendientes solo se escribe 0.
     */
    pair<int, int> consultar_datos(string id) const;
    
    /** @brief Consultar la existencia de un usuario
     \pre cierto
     \post devuleve si existe o no el usuario
     */
    bool existencia_usuario(string id) const;
    
    /** @brief Consultar el usuario con id menor y más procesos pendientes
     \pre cierto
     \post devuleve el id del usuario con más procesos pendientes
     */
    string obtener_id() const;
    
    
    
};

#endif
