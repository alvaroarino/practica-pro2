var searchData=
[
  ['práctica_20de_20pro2_2e_20otoño_202018_2e',['Práctica de PRO2. Otoño 2018.',['../index.html',1,'']]],
  ['p',['p',['../struct_mem_data.html#ae3d6b21233ce467a8c16a33b2065cde2',1,'MemData']]],
  ['p_5fid',['p_id',['../class_proceso.html#a14b00e2942d8a7e5af998d7aa3a20f6f',1,'Proceso']]],
  ['pendientes_5fy_5fmas_5fantiguo',['pendientes_y_mas_antiguo',['../class_usuario.html#ade37330b9a44325fa786a94927c680f9',1,'Usuario']]],
  ['poner_5fproceso',['poner_proceso',['../class_procesador.html#a12a6b4700d4461ed128a5a4ef55efaf3',1,'Procesador']]],
  ['poner_5fproceso_5fen_5fprocesador',['poner_proceso_en_procesador',['../class_cluster.html#a31a9b919f55f5c72e5d53254824b5056',1,'Cluster']]],
  ['poner_5fusuario',['poner_usuario',['../class_cjt__usuarios.html#a461cd3268d19381edee0632a69a1fca8',1,'Cjt_usuarios']]],
  ['pos',['pos',['../struct_mem_data.html#aca7ebf46ffe4a459d852e0304d17d998',1,'MemData']]],
  ['procesador',['Procesador',['../class_procesador.html',1,'Procesador'],['../class_procesador.html#aacfa2a06c7d44636ecfb0338bebbfa8b',1,'Procesador::Procesador()'],['../class_procesador.html#a821edd9119e1b4c3c81f5efed0587512',1,'Procesador::Procesador(int m)']]],
  ['procesador_2ecc',['procesador.cc',['../procesador_8cc.html',1,'']]],
  ['procesador_2ehh',['procesador.hh',['../procesador_8hh.html',1,'']]],
  ['procesadores',['procesadores',['../class_cluster.html#a576abb717193316a8e7db52bf5aa2808',1,'Cluster']]],
  ['proceso',['Proceso',['../class_proceso.html',1,'Proceso'],['../class_proceso.html#a632f57d6ec48e76c80fbeb7734c1148c',1,'Proceso::Proceso()'],['../class_proceso.html#a99a5eab51967f02b9523d22fe99e5fbf',1,'Proceso::Proceso(string id)'],['../class_proceso.html#a27ab814d999f540499367454fe8cce9d',1,'Proceso::Proceso(int mem, string id, int t)']]],
  ['proceso_2ecc',['proceso.cc',['../proceso_8cc.html',1,'']]],
  ['proceso_2ehh',['proceso.hh',['../proceso_8hh.html',1,'']]],
  ['procesos',['procesos',['../class_usuario.html#aaa8c1babdfbbeb3f61367833dc3f3b4c',1,'Usuario']]],
  ['program_2ecc',['program.cc',['../program_8cc.html',1,'']]]
];
