var searchData=
[
  ['pendientes_5fy_5fmas_5fantiguo',['pendientes_y_mas_antiguo',['../class_usuario.html#a639125d3d9a48df6e1904b8dda0bb770',1,'Usuario']]],
  ['poner_5fproceso',['poner_proceso',['../class_procesador.html#a43d32af735ca16dd9947084723e461ab',1,'Procesador']]],
  ['poner_5fproceso_5fen_5fprocesador',['poner_proceso_en_procesador',['../class_cluster.html#a523c61dc9b9c68fc8546e9930f7db4ef',1,'Cluster']]],
  ['poner_5fusuario',['poner_usuario',['../class_cjt__usuarios.html#a461cd3268d19381edee0632a69a1fca8',1,'Cjt_usuarios']]],
  ['procesador',['Procesador',['../class_procesador.html#aacfa2a06c7d44636ecfb0338bebbfa8b',1,'Procesador::Procesador()'],['../class_procesador.html#a821edd9119e1b4c3c81f5efed0587512',1,'Procesador::Procesador(int m)']]],
  ['procesador_5fmaxima_5fmemoria',['procesador_maxima_memoria',['../class_cluster.html#a0a650abd0120a13cd709150a10b76025',1,'Cluster']]],
  ['procesadores_5fcon_5fmisma_5fmemoria',['procesadores_con_misma_memoria',['../class_cluster.html#a4b38ea1ae26e349ddb33facd09a52691',1,'Cluster']]],
  ['proceso',['Proceso',['../class_proceso.html#a632f57d6ec48e76c80fbeb7734c1148c',1,'Proceso::Proceso()'],['../class_proceso.html#a3e5b2ad72c158e4284f5ab28130f5ea6',1,'Proceso::Proceso(int ID)'],['../class_proceso.html#a3b33a3048be14c55dd36b1fae673dad3',1,'Proceso::Proceso(int memoria, int ID, int tiempo)']]],
  ['proceso_5fmas_5fantiguo',['proceso_mas_antiguo',['../class_usuario.html#a4d33539a2bdfc7c96bc81f0eb44adf63',1,'Usuario']]],
  ['procesos_5frechazados',['procesos_rechazados',['../class_usuario.html#add0ade22ac5336986a9499b0a3ebf999',1,'Usuario']]]
];
