var searchData=
[
  ['main',['main',['../program_8cc.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'program.cc']]],
  ['max_5fmem',['MAX_MEM',['../class_procesador.html#a0b4fc56395e4de366bbe0cf68af01c5f',1,'Procesador']]],
  ['mem',['mem',['../class_procesador.html#a3d6ccf8b7d9fe739614816d92dddb17b',1,'Procesador::mem()'],['../class_proceso.html#a2f0077c9db56775d552dc3941a81ec0b',1,'Proceso::mem()']]],
  ['memdata',['MemData',['../struct_procesador_1_1_mem_data.html',1,'Procesador']]],
  ['memit',['MemIt',['../class_procesador.html#a30c79b9dc6345dc9725299a3b1a034c7',1,'Procesador']]],
  ['memitc',['MemItc',['../class_procesador.html#a1e1cefce1f46a21b326f5ffd224eeaa2',1,'Procesador']]],
  ['memoria',['memoria',['../class_proceso.html#a859b2366e5945e401d002ec69e68019e',1,'Proceso']]],
  ['memoria_5flibre',['memoria_libre',['../class_procesador.html#ae9234c9557d45ba1a7c02c83617d250d',1,'Procesador']]],
  ['modificar_5ftiempo',['modificar_tiempo',['../class_proceso.html#ad8017254f55dd1fdcd6e9624f5aae592',1,'Proceso']]],
  ['muser',['muser',['../class_cjt__usuarios.html#ac886616fd1f9a34793699e55527b3674',1,'Cjt_usuarios']]]
];
