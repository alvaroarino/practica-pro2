var searchData=
[
  ['p_5fid',['p_id',['../class_proceso.html#a14b00e2942d8a7e5af998d7aa3a20f6f',1,'Proceso']]],
  ['pendientes_5fy_5fmas_5fantiguo',['pendientes_y_mas_antiguo',['../class_usuario.html#ade37330b9a44325fa786a94927c680f9',1,'Usuario']]],
  ['poner_5fproceso',['poner_proceso',['../class_procesador.html#a12a6b4700d4461ed128a5a4ef55efaf3',1,'Procesador']]],
  ['poner_5fproceso_5fen_5fprocesador',['poner_proceso_en_procesador',['../class_cluster.html#a31a9b919f55f5c72e5d53254824b5056',1,'Cluster']]],
  ['poner_5fusuario',['poner_usuario',['../class_cjt__usuarios.html#a461cd3268d19381edee0632a69a1fca8',1,'Cjt_usuarios']]],
  ['pos_5fmem_5flibre',['pos_mem_libre',['../class_procesador.html#a8cab9c2249dbad45602fb2f3bf811b8c',1,'Procesador']]],
  ['preord_5fpr',['preord_pr',['../cluster_8cc.html#a64de09549cf183848fa0c73d19072adf',1,'cluster.cc']]],
  ['procesador',['Procesador',['../class_procesador.html#aacfa2a06c7d44636ecfb0338bebbfa8b',1,'Procesador::Procesador()'],['../class_procesador.html#a821edd9119e1b4c3c81f5efed0587512',1,'Procesador::Procesador(int m)']]],
  ['proceso',['Proceso',['../class_proceso.html#a632f57d6ec48e76c80fbeb7734c1148c',1,'Proceso::Proceso()'],['../class_proceso.html#a99a5eab51967f02b9523d22fe99e5fbf',1,'Proceso::Proceso(string id)'],['../class_proceso.html#aaaafe06e80c96d2f159c3fe396ad7d7a',1,'Proceso::Proceso(int memoria, string ID, int tiempo)']]],
  ['proceso_5fmas_5fantiguo',['proceso_mas_antiguo',['../class_usuario.html#a4d33539a2bdfc7c96bc81f0eb44adf63',1,'Usuario']]]
];
