//
//  proceso.cc
//  practica-pro2
//
//  Created by Alvaro Ariño Cabau on 19/11/2018.
//  Copyright © 2018 Alvaro Ariño Cabau. All rights reserved.
//

/** @file proceso.cc
 @brief Código de la clase Proceso
 */

#include "proceso.hh"

Proceso::Proceso() {
    mem = 0;
    id = 0;
    t = 0;
}

Proceso::Proceso(int ID) {
    id = ID;
    mem = 0;
    t = 0;
}

Proceso::Proceso(int ID, int memoria, int tiempo) {
    id = ID;
    mem = memoria;
    t = tiempo;
}

void Proceso::modificar_tiempo(int t) {
    this -> t -= t;
}

int Proceso::id_del_proceso() const {
    return this -> id;
}

bool Proceso::empty() const {
    if (mem == 0) {
        return true;
    } else {
        return false;
    }
}

int Proceso::memoria() const {
    return this -> mem;
}

int Proceso::tiempo() const {
    return this -> t;
}

void Proceso::escribir() const {
    cout << id << " " << mem << " " << t << endl;
}
