//
//  cluster.hh
//  practica-pro2
//
//  Created by Alvaro Ariño Cabau on 05/11/2018.
//  Copyright © 2018 Alvaro Ariño Cabau. All rights reserved.
//

/** @file cluster.hh
    @brief Especificación de la clase Cluster
*/
#ifndef cluster_hh
#define cluster_hh

#include "usuario.hh"
#include "proceso.hh"
#include "Cjt_usuarios.hh"
#include "procesador.hh"

#ifndef NO_DIAGRAM
#include <iostream>
#include <vector>
#include <list>
#include "BinTree.hh"
using namespace std;
#endif

/** @class Cluster
    @brief Representa un cluster con n procesadores distribuidos en forma de árbol, con identificadores 1,...,n.
 
    Dispone de un estado posible, no inicializado; para configurar el cluster es necesario el uso de la acción configurar_cluster.
 
    Todas las operaciones son de <b>coste constante</b> salvo las indicadas.
 */

class Cluster {
private:
    // Estructura interna del cluster
    /** @brief Árbol de identificadores */
    BinTree<int> relacion_de_procesadores;
    
    /** @brief Datos de los procesadores */
    vector<Procesador> procesadores;
    
    /** @brief Memoria total del cluster */
    int total_mem_libre;
    
    /** @brief Construcción del árbol del cluster
     \pre cierto
     \post Se construye el árbol de procesadores. Los ids introducidos no són 0. En caso contrario no se introducen en el árbol.
     */
    static BinTree<int> build_tree();
    
    /** @brief Recalcular el total de memoria libre
     \pre cierto
     \post Devuelve el valor de memoria libre total del cluster.
     */
    int sumar_total_mem_libre() const;
    
    /** @brief Desempate de los procesadores en caso de tener la misma memoria
     
     <b>Condiciones para el desempate: </b>
     Se devuelve:
     <um>
     <li> El que esta más cerca de la raiz.</li>
     <li> Si persiste el empate el de mas a la izquierda.</li>
     </um>
     
     \pre cierto
     \post Devuelve el id del procesador escogido con las condiciones del desempate.
     */
    static int desempate(int id1, int id2, int prof1, int prof2);
    
    /** @brief Función de imersión de <em>eleccion_de_procesador</em>.
     
     \pre Tener un cluster configurado.
     \post Se devuelve el id del procesador con más memoria libre.
     */
    
    static int i_eleccion_de_procesador(const BinTree<int>& a, const vector<Procesador>& v, int profundidad_izquierdo, int profundidad_derecha);
    
    /** @brief Búsqueda del procesador para enviar el proceso seleccionado en la función <em>enviar_procesos_a_cluster</em>
     
     \pre Tener un cluster configurado.
     \post Se devuelve el id del procesador con más memoria libre.
     */
    static int eleccion_de_procesador(const BinTree<int>& a, const vector<Procesador>& v);
    
public:
    
    // Constructores
    
    /** @brief Creadora por defecto.
     
        Se ejecuta automáticamente al declarar un cluster.
        \pre cierto
        \post el resultado es un cluster sin ningún usuario o proceso asociado
    */
    Cluster();

    // Modificadores
    /** @brief Configurar un cluster
     
     Se leerá el número de procesadores, sus conexiones y la memoria de cada uno de
     ellos en orden creciente de id.
     Si se vuelve a configurar el cluster mas adelante, los procesos activos se eliminan,
     pero los procesos pendientes de los usuarios se conservan para el siguiente cluster
     en el mismo estado que antes.
     
     \pre cierto
     \post El resultado es un cluster configurado con el número de procesadores asociados, las conexiones y la memora de cada procesaodr
     */
    void configurar_cluster();
    
    /** @brief Modificadora para añadir un proceso al procesador.
     
     \pre Existe el procesador y usuario indicado. Si el usuario no existe no se hace nada.
     \post Si hay un hueco de memoria libre consecutiva en el que quepa el proceso,
     este pasa a ejecutarse en el procesador indicado y la memoria que use pasa a estar ocupada.
     Si el proceso no cabe, pasa a ser el proceso pendiente mas reciente del usuario.
     */
    void poner_proceso_en_procesador(Proceso& p, Cjt_usuarios& cjt, string id, int id_del_procesador);
    
    /** @brief Modificadora para quitar un proceso al procesador.
     
     \pre Existe el procesador y el proceso indicado. Si no existe el proceso no se hace nada.
     \post Si existe el proceso en el procesador indicado, la memoria usada por el proceso queda libre y este desaparece.
     */
    void quitar_proceso_de_procesador(int id_proceso, int id_del_procesador);
    
    /** @brief Avanzar el tiempo en el cluster.
     
     \pre Tener un cluster configurado.
     \post El parámetro implícito contiene todos los procesos salvo aquellos que estaban activos que hayan acabado en el intervalo de tiempo indicado desde el momento inicial.
     */
    void avanzar_tiempo(int t);
    
    /** @brief Envío de procesos al cluster.
     
     \pre Tener un cluster configurado.
     \post El parámetro implicito contiene los procesos que hayan cabido, bien hasta que haya np procesos o cuando no se puedan colocar más.
     */
    void enviar_procesos_a_cluster(int np, Cjt_usuarios& cjt);
    
    // Consultoras
    /** @brief Consultar el estado del procesador
     
     Permite consultar los procesos que hay activos en el procesador con <b><em>id_del_procesador</em></b> en ese momento.
     
     \pre Debe existir almenos un proceso. En caso contrario no se hace nada.
     \post Se escribe por orden de inicio de memoria, la posicion y el resto de datos de cada proceso.
     El tiempo escrito es el que falta para que el proceso acabe, no el tiempo inicial de ejecución.
     */
    void consultar_procesador(int id_del_procesador) const;
    
};

#endif 
