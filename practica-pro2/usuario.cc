//
//  usuario.cc
//  practica-pro2
//
//  Created by Alvaro Ariño Cabau on 19/11/2018.
//  Copyright © 2018 Alvaro Ariño Cabau. All rights reserved.
//

/** @file usuario.cc
 @brief Código de la clase Usuario
 */

#include "usuario.hh"

Usuario::Usuario() {
    procesos = queue<Proceso>();
    pendientes = 0;
    rechazados = 0;
}


void Usuario::enviar_proceso_a_usuario(const Proceso& p) {
    ++pendientes;
    procesos.push(p);
}

void Usuario::enviar_proceso_rechazado(const Proceso& p) {
    ++rechazados;
    procesos.push(p);
}

Proceso Usuario::proceso_mas_antiguo() {
    Proceso p = procesos.front();
    procesos.pop();
    --pendientes;
    return p;
}

void Usuario::reset_rechazados() {
    pendientes = int(procesos.size());
    rechazados = 0;
}

int Usuario::procesos_rechazados() const {
    return this -> rechazados;
}

pair<int, int> Usuario::pendientes_y_mas_antiguo() const {
    int b = 0;
    if (not procesos.empty()) b = procesos.front().id_del_proceso();
    return make_pair(pendientes, b);
}

bool Usuario::empty() const {
    return procesos.empty();
}
