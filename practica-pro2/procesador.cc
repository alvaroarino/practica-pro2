//
//  procesador.cc
//  practica-pro2
//
//  Created by Alvaro Ariño Cabau on 19/11/2018.
//  Copyright © 2018 Alvaro Ariño Cabau. All rights reserved.
//

/** @file procesador.cc
 @brief Código de la clase Procesador
 */

#include "procesador.hh"

void Procesador::recalcular_mem_libre() {
    int m = 0;
    
    for (MemIt it = mem.begin(); it != mem.end(); ++it) {
        if (not it -> vacia) {
            // Ocupados
            m += ((it -> end) - (it -> pos)) + 1;
        }
    }
    
    tam_mem_libre = MAX_MEM - m;
}

void Procesador::desfragmentar_memoria() {
    MemIt fin = mem.end();
    --fin;
    
    for (MemIt it1 = mem.begin(); it1 != fin; ++it1) {
        MemIt it2 = it1;
        ++it2;
        if (it2 != mem.end() and (it1 -> vacia and it2 -> vacia)) it1 -> end = it2 -> end;
    }
}

Procesador::Procesador() {
    mem = list<MemData>();
    MAX_MEM = 0;
    tam_mem_libre = MAX_MEM;
    
    MemData x;
    x.p = Proceso();
    x.user = "";
    
    x.pos = 0;
    x.end = 0;
    
    x.vacia = true;
    
    mem.insert(mem.end(), x);
}

Procesador::Procesador(int m) {
    mem = list<MemData>();
    MAX_MEM = m;
    tam_mem_libre = MAX_MEM;
    
    MemData x;
    x.p = Proceso();
    x.user = "";
    
    x.pos = 0;
    x.end = m - 1;
    
    x.vacia = true;

    mem.insert(mem.end(), x);
}

void Procesador::poner_proceso(Proceso& proceso_nuevo, Cjt_usuarios& cjt, string id) {
    if (cjt.existencia_usuario(id)) {

        MemIt it = mem.begin();
        bool added = false;
        
        while (it != mem.end()) {
            if (it -> vacia) {
                MemData fragmento;
                fragmento.p = proceso_nuevo;
                fragmento.user = id;
                fragmento.vacia = false;
                
                fragmento.pos = it -> pos;
                fragmento.end = fragmento.pos + fragmento.p.memoria() - 1;
                it -> pos = fragmento.end + 1;
                mem.insert(it, fragmento);
                
                added = true;
                
                recalcular_mem_libre();
            }
            if (added) it = mem.end();
            else ++it;
        }
        
        if (not added) {
            cjt.enviar_proceso_a_usuario(proceso_nuevo, id);
        }
    }
}

void Procesador::quitar_proceso(int id_proceso) {
    MemIt it = mem.begin();
    bool erased = false;
    
    while (it != mem.end()) {
        if ((it -> p.id_del_proceso()) == id_proceso) {
            // Cambiamos los datos por una posición de memoria vacia
            it -> p = Proceso();
            it -> user = "";
            it -> vacia = true;
            erased = true;
        }
        if (erased) {
            desfragmentar_memoria();
            recalcular_mem_libre();
            it = mem.end();
        } else {
           ++it;
        }
    }
    
}

void Procesador::avanzar_tiempo_procesador(int t) {
    for (MemIt it = mem.begin(); it != mem.end(); ++it) {
        it -> p.modificar_tiempo(t);
        if (it -> p.tiempo() <= 0) {
            // Si el tiempo del fragmento es menor o igual al tiempo avanzado, este se quita del procesador.
            quitar_proceso(it -> p.id_del_proceso());
        }
    }
}

bool Procesador::empty() const {
    if (memoria_libre() == MAX_MEM) return true;
    else return false;
}

bool Procesador::consultar_memoria(Proceso& p) const {
    
    for (MemItc it = mem.begin(); it != mem.end(); ++it) {
        int tam = ((it -> end) - (it -> pos)) + 1;
        if (tam == p.memoria()) return true;
        else if (tam > p.memoria()) return true;
    }
    return false;
    
}

int Procesador::memoria_libre() const {
    return this -> tam_mem_libre;
}

void Procesador::escribir() const {
    for (MemItc it = mem.begin(); it != mem.end(); ++it) {
        if (not it -> vacia) { // Evita que las posiciones vacias de memoria se impriman
            cout << "  " << it -> pos << " " << it -> user << " ";
            it -> p.escribir();
        }
    }
}
