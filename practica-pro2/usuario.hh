//
//  usuario.hh
//  practica-pro2
//
//  Created by Alvaro Ariño Cabau on 04/11/2018.
//  Copyright © 2018 Alvaro Ariño Cabau. All rights reserved.
//

/** @file usuario.hh
    @brief Especificación de la clase Usuario
*/
#ifndef usuario_hh
#define usuario_hh

#include "proceso.hh"

#ifndef NO_DIAGRAM
#include <iostream>
#include <queue>
using namespace std;
#endif

/** @class Usuario
    @brief Representa un usuario que tiene un identificador formado por minúsculas y números.
 
 Este comienza por una letra
 
 Dispone de dos estados posibles (inicializada / no inicializada); si está inicializada tiene un id asociado; si no está inicializada el id vale 0.
 
 Todas las operaciones son de <b>coste constante</b> salvo las indicadas. */

class Usuario {
private:
	
	/** @brief Cola de procesos pendientes*/
	queue<Proceso> procesos;

	/** @brief Procesos pendientes*/
	int pendientes;
	
	/** @brief Procesos rechazados*/
	int rechazados;
    
public:
    
    // Constructores
    /** @brief Creadora por defecto.
     
     Se ejecuta automáticamente al declarar un usuario.
     \pre <em>cierto</em>
     \post el resultado es un usuario sin ningún id o procesos asociados
     */
    Usuario();
	
    // Modificadores
    /** @brief Envio de procesos a los usuarios
      \pre el proceso no esta
      \post El parámetro implícito pasa a tener los datos de un proceso y este pasa a ser un proceso pendiente más reciente.
    */
    void enviar_proceso_a_usuario(const Proceso& p);
	
	/** @brief Envio de los procesos rechazados al usuario
	 \pre el proceso no esta
	 \post El parámetro implícito pasa a tener los procesos que han sido rechazados en la función <em>enviar_proceso_a_cluster</em>.
	 */
	void enviar_proceso_rechazado(const Proceso& p);
	
	/** @brief Consultar el proceso mas antiguo
	 \pre cierto
	 \post Devuelve el proceso mas antiguo del parametro implicito y lo elimina.
	 */
	Proceso proceso_mas_antiguo();
	
	/** @brief Reinicializar la cuenta de procesos rechazados
	 \pre el proceso no esta
	 \post Suma los procesos rechazados a los pendientes i reinicia el contador de rechazados.
	 */
	void reset_rechazados();
	
	// Consultoras	
	/** @brief Número de procesos rechazados
	 
	 \pre <em>cierto</em>
	 \post Devuelve el número de procesos rechazados
	 */
	int procesos_rechazados() const;
	
	/** @brief Consultar valores asociados al parametro implicito
	 
	 \pre <em>cierto</em>
	 \post El resultado es cuantos procesos pendientes tiene y el identificador del mas antiguo.
	 */
	pair<int, int> pendientes_y_mas_antiguo() const;
	
	/** @brief Consultar si el usuario no tiene ningún proceso pendiente
	 
	 \pre cierto
	 \post Devuelve true si el usuario no tiene procesos pendientes
	 */
	bool empty() const;	
	
};

#endif
